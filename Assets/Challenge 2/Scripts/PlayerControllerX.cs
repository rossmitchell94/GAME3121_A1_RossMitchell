﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;
    private bool spawnReady = true;

    // Update is called once per frame
    void Update()
    {
        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (spawnReady)
            {
                Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
                StartCoroutine(SpawnDelay());
            }
        }
    }

    IEnumerator SpawnDelay()
    {
        spawnReady = false;
        yield return new WaitForSeconds(0.5f);
        spawnReady = true;
    }
}
